from py4j.java_gateway import JavaGateway

# We have to connect toa a Py4J server hosted by se.lu.esss.ics.py4jserver application
gateway = JavaGateway()
jvm     = gateway.jvm   # the JVM object is used to access Java classes

# We'll define some short names for things used troughout the script
Py4JServer          = jvm.se.lu.esss.ics.py4jserver.Py4JServer
XMLDataManager      = jvm.xal.smf.data.XMLDataManager
AlgorithmFactory    = jvm.xal.sim.scenario.AlgorithmFactory
Scenario            = jvm.xal.sim.scenario.Scenario

SEQ_ID      = 'mebt'
START_ID    = 'MEBT-PBO_QV-1'
END_ID      = 'MEBT-PBO_QV-4'

accelerator_seq = XMLDataManager.loadDefaultAccelerator()
sequence = accelerator_seq.getSequence(SEQ_ID)

start_node = sequence.getNodeWithId(START_ID)
print 'Start node position is', sequence.getPosition(start_node)

# create and initialize an algorithm

etracker = AlgorithmFactory.createEnvTrackerAdapt(sequence)

# set some custom parameters
etracker.setMaxIterations(10000)
etracker.setAccuracyOrder(1)
etracker.setErrorTolerance(0.001)

# create and initialize a probe
# have to use wrapper in the Java gateway
probe = Py4JServer.getEnvelopeProbe(sequence, etracker)
probe.setBeamCurrent(0.038);
probe.setKineticEnergy(885.e6);
probe.setSpeciesCharge(-1.0);
probe.setSpeciesRestEnergy(939.29e6)

probe.initialize()

# Create and initialize the model to the target sequel
model = Scenario.newScenarioFor(sequence)

# Set the probe to simulate, the synchronization model, and the starting node
model.setProbe(probe)
model.setSynchronizationMode(Scenario.SYNC_MODE_DESIGN)
model.resync()
model.setStartNode(START_ID)

#
# Run the Model
#
model.run()

# Retrieve the probe from the model then the trajectory object from the probe
probe = model.getProbe()        # this isn't necessary since we already have it
trajectory = probe.getTrajectory()
probe_states = Py4JServer.listFromEnvelopeTrajectory(trajectory)

# Retrieve the final state of the simulation
final_state = trajectory.finalState()

# If the probe is an EnvelopeProbe, we can get the covariance matrix of the final state
final_cov_matrix =  final_state.getCovarianceMatrix()

# We can get all the covariance matrices of the trajectory
trajectory_cov_matrices = []
for probe_state in probe_states:
    cov_matrix = probe_state.getCovarianceMatrix()
    trajectory_cov_matrices.append( cov_matrix )

# We can print out all the envelope sizes
print "Envelope  sigX      sigY      sigZ"
for cov_matrix in trajectory_cov_matrices:
    print cov_matrix.getSigmaX(), cov_matrix.getSigmaY(), cov_matrix.getSigmaZ()

