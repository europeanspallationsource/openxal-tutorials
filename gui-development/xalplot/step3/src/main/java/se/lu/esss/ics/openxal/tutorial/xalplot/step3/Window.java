package se.lu.esss.ics.openxal.tutorial.xalplot.step3;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import xal.extension.application.XalWindow;
import xal.extension.widgets.plot.FunctionGraphsJPanel;

/**
 * It defines a window for the document.
 * The document window class must define the main window used to present the document to the user.
 * The developer is free to lay out the window as desired
 */
public class Window extends XalWindow {
	private static final long serialVersionUID = 1L;

	protected JTabbedPane tabbedPane;
	protected FunctionGraphsJPanel plot;
	protected JTable dataTable;
	
	/**
	 * Creates a window presenting the document
	 * @param document The document to load
	 */
	public Window(Document document) {
		super(document);
		setSize(600, 600);
		makeContent(document);
	}	

	/**
	 * Lays out the window.
	 */
	protected void makeContent(Document document) {
		tabbedPane = new JTabbedPane();

		JTable dataTable = new JTable(makeTableModel(document));
		JPanel dataTablePanel = new JPanel(new BorderLayout());
		dataTablePanel.add(new JScrollPane(dataTable));
		tabbedPane.addTab("Data", dataTablePanel);

		plot = new FunctionGraphsJPanel();
		plot.addGraphData(document.data);
		tabbedPane.addTab("Plot", plot);

		getContentPane().add(tabbedPane);

	}

	private TableModel makeTableModel(final Document document) {
		return new AbstractTableModel() {
			@Override
			public String getColumnName(int columnIndex) {
				switch (columnIndex) {
				case 0:
					return "x";
				case 1:
					return "y";
				default:
					throw new IllegalArgumentException();
				}
			}

			private static final long serialVersionUID = 1L;

			@Override
			public int getColumnCount() {
				return 2;
			}

			@Override
			public int getRowCount() {
				return document.data.getNumbOfPoints();
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				switch (columnIndex) {
				case 0:
					return document.data.getX(rowIndex);
				case 1:
					return document.data.getY(rowIndex);
				default:
					throw new IllegalArgumentException();
				}
			}
		};
	}
}
