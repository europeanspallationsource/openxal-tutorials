package se.lu.esss.ics.openxal.tutorial.xalplot.step2;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import xal.extension.application.Application;
import xal.extension.application.ApplicationAdaptor;
import xal.extension.application.XalDocument;

/**
 * The Main class is an entry point for the application.
 * This is a frame application, i.e. containing single window with single document. 
 * */
public class Main extends ApplicationAdaptor {
	protected String[] documentTypes = new String[]{"dat"};
		
	/**
	 * Returns a new instance of application's document class 
	 * @see xal.application.DesktopApplicationAdaptor#newEmptyDocument()
	 * 
	 * @return application's document class
	 */
	@Override
	public XalDocument newEmptyDocument() {
		return new Document();
	}

	/** Returns an instance of application's document class with specific file 
	 * @see xal.application.DesktopApplicationAdaptor#newDocument(java.net.URL)
	 * 
	 * @param url Path to the file to be opened
	 * @return application's document class with the file opened
	 */
	@Override
	public XalDocument newDocument(URL url) {
		return Document.load(url);
	}

	/** 
	 * Returns an array of file extensions of readable files.
	 * @see xal.application.AbstractApplicationAdaptor#readableDocumentTypes()
	 * 
	 * @return array of file extensions of writable files
	 */
	@Override
	public String[] readableDocumentTypes() {
		return documentTypes;
	}

	/**
	 * Returns an array of file extensions of writable files
	 * @see xal.application.AbstractApplicationAdaptor#writableDocumentTypes()
	 * 
	 * @return array of file extensions of writable files
	 */
	@Override
	public String[] writableDocumentTypes() {
		return new String[]{};
	}

	/**
	 * Returns application name
	 * @see xal.application.AbstractApplicationAdaptor#applicationName()
	 * 
	 * @return the application name
	 */
	@Override
	public String applicationName() {		
		return "XalPlot";
	}
	
	/**
	 * Main entry point of the application
	 * 
	 * @param args Application arguments
	 */
	public static void main(String[] args) {
		try {
		    setOptions( args );
		    System.out.println("Starting application...");
		    Logger.getLogger("global").log( Level.INFO, "Starting application..." );
		    Application.launch(new Main());
		}
		catch(Exception exception) {
		    Logger.getLogger("global").log( Level.SEVERE, "Error starting application.", exception );
		    System.err.println( exception.getMessage() );
		    exception.printStackTrace();
		    Application.displayApplicationError("Launch Exception", "Launch Exception", exception);
		    System.exit(-1);
		}
	}
	
}
