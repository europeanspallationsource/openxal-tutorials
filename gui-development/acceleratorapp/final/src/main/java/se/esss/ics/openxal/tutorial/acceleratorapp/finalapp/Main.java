package se.esss.ics.openxal.tutorial.acceleratorapp.finalapp;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import xal.extension.application.Application;
import xal.extension.application.ApplicationAdaptor;
import xal.extension.application.XalDocument;
import xal.extension.application.smf.AcceleratorApplication;

/**
 * The Main class is an entry point for the application.
 * This is an accelerator application.
 * */
public class Main extends ApplicationAdaptor {
    protected String[] documentTypes = new String[]{"sedt"};

    /**
     * Returns a new instance of application's document class
     *
     * @return application's document class
     */
    @Override
    public XalDocument newEmptyDocument() {
        return new Document();
    }

    /** Returns an instance of application's document class with specific file
     *
     * @param url Path to the file to be opened
     * @return application's document class with the file opened
     */
    @Override
    public XalDocument newDocument(URL url) {
        return Document.load(url);
    }

    /**
     * Returns an array of file extensions of readable files.
     *
     * @return array of file extensions of writable files
     */
    @Override
    public String[] readableDocumentTypes() {
        return writableDocumentTypes();
    }

    /**
     * Returns an array of file extensions of writable files
     *
     * @return array of file extensions of writable files
     */
    @Override
    public String[] writableDocumentTypes() {
        return documentTypes;
    }

    /**
     * Returns application name
     *
     * @return the application name
     */
    @Override
    public String applicationName() {
        return "Simulation editor";
    }

    /**
     * Main entry point of the application
     *
     * @param args Application arguments
     */
    public static void main(String[] args) {
        try {
            setOptions( args );
            System.out.println("Starting application...");
            Logger.getLogger("global").log( Level.INFO, "Starting application..." );
            AcceleratorApplication.launch(new Main());
        }
        catch(Exception exception) {
            Logger.getLogger("global").log( Level.SEVERE, "Error starting application.", exception );
            System.err.println( exception.getMessage() );
            exception.printStackTrace();
            Application.displayApplicationError("Launch Exception", "Launch Exception", exception);
            System.exit(-1);
        }
    }
}
